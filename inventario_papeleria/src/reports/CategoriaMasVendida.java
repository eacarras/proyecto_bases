package reports;

public class CategoriaMasVendida {
	
	// Attributes
	private String nombre;
	private String total;
	
	// Constructor
	public CategoriaMasVendida(String nombre, String total) {
		this.nombre = nombre;
		this.total = total;
	}
	
	//  Getters and setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}
}
