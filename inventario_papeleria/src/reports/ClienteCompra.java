package reports;

public class ClienteCompra {
	
	// Attributes
	private String nombre;
	private String total;
	
	// Constructor
	public ClienteCompra(String nombre, String total) {
		this.nombre = nombre;
		this.total = total;
	}
	
	// Getter and setter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}
}
