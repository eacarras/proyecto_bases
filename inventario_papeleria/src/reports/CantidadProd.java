package reports;

public class CantidadProd {
	
	// Attributes
	private String nombre;
	private String stock;
	private String empresa;
	
	// Constructor
	public CantidadProd(String nombre, String stock, String empresa) {
		this.nombre = nombre;
		this.stock = stock;
		this.empresa = empresa;
	}
	
	// Getter and setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
}
