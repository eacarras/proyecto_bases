package reports;

public class ClienteMayorF {
	
	// Attributes
	// In the query the first value is the id of the client
	private String nombre;
	private String total;
	
	// Constructor
	public ClienteMayorF(String nombre, String total) {
		this.nombre = nombre;
		this.total = total;
	}
	
	// Getter and Setter
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}
}
