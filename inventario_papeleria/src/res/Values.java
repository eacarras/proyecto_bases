package res;

public class Values {
	
	// Dimensions
	private static double WIDTH_WINDOW = 626.0;
	public static double WIDTH_WINDOW_HOME = 876.0;
	public static double WIDTH_WINDOW_REPORTS = 900.0;
	private static double HEIGHT_WINDOW = 415.0;
	public static double HEIGHT_WINDOW_HOME = 665.0;
	public static double HEIGHT_WINDOW_REPORTS = 600.0;
	private static double WIDTH_SMALL_WINDOW = 320.0;
	private static double HEIGHT_SMALL_WINDOW = 180.0;
	
	// Button Dimensions
	private static double SPACING_BUTTONS = 15.0;
	private static double SPACING_BUTTONS_HOME = 90.0;
	
	// String for manage information
	private static String VALUES_FOR_INSERT = "VALUES";
	private static String VALUES_FOR_QUERY = "FROM";
	
	// Array for reports
	public static String[] array = {"Cantidad de Productos","Cliente con mayor frecuencia de compra", 
	                                         "Top 5 producto mas vendidos", "Clientes y cantidad de compra",
	                                         "Producto con menor numero de ventas", "Categoria con mayor productos vendidos"};
	
	// Important String
	private static String NAME_COMPANY = "Papeleria Andrea";
	
	// Methods of access
	public static double getWIDTH_WINDOW() {
		return WIDTH_WINDOW;
	}
	
	public static double getHEIGHT_WINDOW() {
		return HEIGHT_WINDOW;
	}
	
	public static double getWIDTH_SMALL_WINDOW() {
		return WIDTH_SMALL_WINDOW;
	}
	
	public static double getHEIGHT_SMALL_WINDOW() {
		return HEIGHT_SMALL_WINDOW;
	}
	
	public static double getSPACING_BUTTONS() {
		return SPACING_BUTTONS;
	}
	
	public static double getSPACING_BUTTONS_HOME() {
		return SPACING_BUTTONS_HOME;
	}
	
	public static String getVALUES_FOR_INSERT() {
		return VALUES_FOR_INSERT;
	}
	
	public static String getVALUES_FOR_QUERY() {
		return VALUES_FOR_QUERY;
	}
	
	public static String getNAME_COMPANY() {
		return NAME_COMPANY;
	}
}
