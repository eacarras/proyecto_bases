package window;

import res.Values;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.layout.AnchorPane;

import javafx.event.ActionEvent;

import javafx.scene.control.Label;
import javafx.scene.control.Button;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ConfirmationWindow extends Application{
	
	// String for user
	private String user;
	
	// Root
	private AnchorPane root;
	
	// Background
	private ImageView background;
	
	// Label and Confirmation Button
	private Label lb_mensaje;
	private Button bt_confirmation;
	
	// Boolean
	private boolean isAuth;
	private Boolean bool;
	
	// Constructor
	public ConfirmationWindow(String string, String auth, String username, Boolean bool) {
		background = new ImageView(new Image("file:src/res/background-confirmation.jpg"));
		
		root = new AnchorPane();
		root.getChildren().add(background);
		
		user = username;
		
		bt_confirmation = new Button("Confirmar");
		this.lb_mensaje = new Label(string);
		this.bool = bool;
		lb_mensaje.setStyle("-fx-font-weight: bold");
		this.isAuth = auth.equals("AUTH") ? true : false;
	}
	
	@Override
	public void start(Stage stage) {
		// Set button
		bt_confirmation.setOnAction((ActionEvent e)-> {
			if(isAuth) {
				HomeActivity home = new HomeActivity(user, bool);
				home.start(stage);
			} else {
				PrincipalWindow principal = new PrincipalWindow();
				principal.start(stage);
			}
		});
		
		AnchorPane.setLeftAnchor(bt_confirmation, 215.0);
		AnchorPane.setBottomAnchor(bt_confirmation, 20.0);
		
		// Set position of the label
		AnchorPane.setLeftAnchor(lb_mensaje, 20.0);
		AnchorPane.setTopAnchor(lb_mensaje, 20.0);
		
		// Add to root 
		root.getChildren().addAll(lb_mensaje, bt_confirmation);
		
		// Create the scene
		Scene scene =  new Scene(root, Values.getWIDTH_SMALL_WINDOW(),
				Values.getHEIGHT_SMALL_WINDOW());
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.setResizable(false);
		stage.show();
	}
}
