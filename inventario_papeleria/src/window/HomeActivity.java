package window;

import res.Values;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

public class HomeActivity extends Application {

	// Root, HBox's and String
	private AnchorPane root;
	
	private HBox general_buttons;
	
	private VBox first_column;
	private VBox second_column;
	
	private String user;
	
	// Label
	private Label lbname;
	
	// Background
	private ImageView background;
	
	// First column of buttons
	private Button btcliente;
	private Button btproveedor;
	private Button bttelefono;
	private Button btdireccion;
	private Button btreports;
	
	// Second column of buttons
	private Button btcompra;
	private Button btproducto;
	private Button btcategoria;
	private Button add_user;
	
	// Button logout
	private Button salir;
	
	// Boolean values
	private boolean value;
	
	// Constructor
	public HomeActivity(String user, boolean value) {
		// Set background
		background = new ImageView(new Image("file:src/res/background.jpg"));
		
		// Roots
		root = new AnchorPane();
		root.getChildren().add(background);
		
		// Label
		lbname = new Label("Bienvenido usuario: " + user);
		AnchorPane.setTopAnchor(lbname, 150.0);
		AnchorPane.setLeftAnchor(lbname, 25.0);
		root.getChildren().add(lbname);
		
		// Initialize all the buttons
		btcliente = new Button("Cliente");
		btproveedor = new Button("Proveedor");
		bttelefono = new Button("Telefono");
		btdireccion = new Button("Direccion");
		btreports = new Button("Reportes");
		btcompra = new Button("Compra");
		btproducto = new Button("Producto");
		btcategoria = new Button("Categoria");
		add_user = new Button("Agregar Usuario");
		
		// Instance VBox and HBox
		first_column = new VBox();
		second_column = new VBox();
		general_buttons = new HBox();
		generatedButtons();
		styleButtons();
		
		// Logout
		salir = new Button("Salir");
		AnchorPane.setRightAnchor(salir, 20.0);
		AnchorPane.setBottomAnchor(salir, 10.0);
		
		// Query for user
		this.user = user;
		this.value = value;
		if(!value) add_user.setVisible(false);
	}
	
	@Override
	public void start(Stage stage) {
		// Logout
		salir.setOnAction(e -> {
			PrincipalWindow logout = new PrincipalWindow();
			logout.start(stage);
		});
		
		// Actions of buttons
		btcliente.setOnAction(e -> {
			goModificationWindow(stage, "cliente");
		});

		btproveedor.setOnAction(e -> {
			goModificationWindow(stage, "proveedor");
		});
		
		bttelefono.setOnAction(e -> {
			goModificationWindow(stage, "telefono");
		});
		
		btdireccion.setOnAction(e -> {
			goModificationWindow(stage, "direccion");
		});
		
		btreports.setOnAction(e -> {
			goReportVideo(stage);
		});
		
		btcompra.setOnAction(e -> {
			goModificationWindow(stage, "compra");
		});
		
		btproducto.setOnAction(e -> {
			goModificationWindow(stage, "producto");
		});
		
		btcategoria.setOnAction(e -> {
			goModificationWindow(stage, "categoria");
		});
		
		add_user.setOnAction(e -> {
			AgregarUsuario add = new AgregarUsuario(user);
			add.start(stage);
		});
		
		// Add into the root
		root.getChildren().addAll(general_buttons, salir);
		
		// Upload the root
		Scene scene = new Scene(root, Values.getWIDTH_WINDOW(), Values.getHEIGHT_WINDOW());
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.setTitle("Home Activity");
		stage.setResizable(false);
		stage.show();
	}
	
	private void generatedButtons() {
		// Put all the buttons into the roots
		first_column.getChildren().addAll(btcliente, btproveedor, bttelefono,
				btdireccion, btreports);
		first_column.setSpacing(20.0);
		second_column.getChildren().addAll(btcompra, btproducto, btcategoria,
				add_user);
		second_column.setSpacing(20.0);
		
		general_buttons.getChildren().addAll(first_column, second_column);
		general_buttons.setSpacing(120.0);
		
		AnchorPane.setTopAnchor(general_buttons, 190.0);
		AnchorPane.setLeftAnchor(general_buttons, 25.0);
	}
	
	private void styleButtons() {
		btcliente.setStyle("-fx-pref-width: 165px");
		btproveedor.setStyle("-fx-pref-width: 165px");
		bttelefono.setStyle("-fx-pref-width: 165px");
		btdireccion.setStyle("-fx-pref-width: 165px");
		btreports.setStyle("-fx-pref-width: 165px");
		btcompra.setStyle("-fx-pref-width: 165px");
		btproducto.setStyle("-fx-pref-width: 165px");
		btcategoria.setStyle("-fx-pref-width: 165px");
		add_user.setStyle("-fx-pref-width: 165px");
	}
	
	private void goModificationWindow(Stage stage, String name) {
		ModificationWindow newWindow = new ModificationWindow(name, value, user);
		newWindow.start(stage);
	}
	
	private void goReportVideo(Stage stage) {
		ReportesView reportes_window = new ReportesView(this.user, this.value);
		reportes_window.start(stage);
	}
}
