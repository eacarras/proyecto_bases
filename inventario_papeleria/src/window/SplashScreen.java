package window;

import res.Values;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.layout.AnchorPane;

import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javafx.scene.text.Font;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class SplashScreen extends Application{
	
	// Root and HBox
	private AnchorPane root;
	
	// ImageView for the background
	private ImageView background;
	
	// Button to go to principal window and Label
	private Button goPrincipalWindow;
	private Label nameCompany;
	
	// Constructor
	public SplashScreen() {
		// Initialize root
		root = new AnchorPane();
		
		// Add background
		background = new ImageView(new Image("file:src/res/background.jpg"));
		
		// Other components
		goPrincipalWindow = new Button("Ingresar");
		
		nameCompany = new Label(Values.getNAME_COMPANY());
		nameCompany.setFont(new Font("Ariel", 30));
	}
	
	@Override
	public void start(Stage stage) {
		// Add background to the root
		root.getChildren().add(background);
		
		// Logic of the button
		goPrincipalWindow.setOnAction(e -> {
			PrincipalWindow nextWindow = new PrincipalWindow();
			nextWindow.start(stage);
		});
		
		// Adding other components to the root
		AnchorPane.setLeftAnchor(nameCompany, 120.0);
		AnchorPane.setTopAnchor(nameCompany, 200.0);
		
		AnchorPane.setLeftAnchor(goPrincipalWindow, 400.0);
		AnchorPane.setTopAnchor(goPrincipalWindow, 205.0);
		
		root.getChildren().addAll(nameCompany, goPrincipalWindow);
		
		Scene scene = new Scene(root, Values.getWIDTH_WINDOW() ,
				Values.getHEIGHT_WINDOW());
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.setResizable(false);
		stage.show();
	}
	
	// Method to start everything
	public static void main(String[] args) {
		launch(args);
	}
}
