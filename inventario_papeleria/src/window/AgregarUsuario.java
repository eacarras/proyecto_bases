package window;

import res.Values;
import database.ConnectionDatabase;
import javafx.application.Application;
import javafx.stage.Stage;

import javafx.scene.Scene;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class AgregarUsuario extends Application{
	
	// Root, HBox, VBox
	private AnchorPane root;
	
	private HBox first_row;
	private HBox second_row;
	
	private VBox general_information;
	
	private String username;
	
	// Background
	private ImageView background;
	
	// Buttons
	private Button cancel;
	private Button register;
	
	// Labels and TextView to get the information
	private Label lbuser;
	private TextField user;
	
	private Label lbpassword;
	private TextField pass;
	
	private CheckBox cbadmin;
	
	private Label lberror;
	
	// Constructor
	public AgregarUsuario(String username) {
		// Background
		this.username = username;
		background = new ImageView(new Image("file:src/res/background.jpg"));
		
		// Initialize all
		root = new AnchorPane();
		root.getChildren().add(background);
		
		first_row = new HBox();
		second_row = new HBox();
		general_information = new VBox();
		
		lbuser = new Label("Usuario:      ");
		user = new TextField();
		
		lbpassword = new Label("Contrasena");
		pass = new TextField();
		
		register = new Button("Registrar nuevo usuario");
		cbadmin = new CheckBox("El usuario es administrador");
		manageComponents();
		
		lberror = new Label("Por favor ingrese un correcto password...");
		AnchorPane.setBottomAnchor(lberror, 20.0);
		AnchorPane.setLeftAnchor(lberror, 20.0);
		lberror.setVisible(false);
		
		cancel = new Button("Regresar");
		AnchorPane.setBottomAnchor(cancel, 20.0);
		AnchorPane.setRightAnchor(cancel, 20.0);
	}
	
	@Override
	public void start(Stage stage) {
		// Logic of the buttons
		cancel.setOnAction(e -> {
			HomeActivity returnhome = new HomeActivity(username, true);
			returnhome.start(stage);
		});
		
		register.setOnAction(e -> {
			validateRegister();
		});
		
		// Set Values into the root
		root.getChildren().addAll(cancel, general_information, lberror);
		
		Scene scene = new Scene(root, Values.getWIDTH_WINDOW(),
				Values.getHEIGHT_WINDOW());
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.setResizable(false);
		stage.show();
	}
	
	private void manageComponents() {
		first_row.getChildren().addAll(lbuser, user);
		first_row.setSpacing(Values.getSPACING_BUTTONS());
		
		second_row.getChildren().addAll(lbpassword, pass);
		second_row.setSpacing(Values.getSPACING_BUTTONS());
		
		general_information.getChildren().addAll(first_row, second_row,
				cbadmin, register);
		general_information.setSpacing(15.0);
		AnchorPane.setTopAnchor(general_information, 150.0);
		AnchorPane.setLeftAnchor(general_information, 20.0);
	}
	
	private void validateRegister() {
		if(pass.getText().isEmpty() || pass.getText().length() < 8)	lberror.setVisible(true);
		else if(user.getText().isEmpty()) {
			lberror.setText("Por favor ingrese todos los campos para continuar");
			lberror.setVisible(true);
		} else {
			String newuser = "\"" + user.getText() + "\", \"" + pass.getText() + "\", ";
			if(cbadmin.isSelected()) newuser = newuser + "1" ;
			else newuser = newuser + "0";
			ConnectionDatabase.addUser(newuser);
			cleanNodes();
		}
	}
	
	private void cleanNodes() {
		user.setText("");
		pass.setText("");
	}
}
