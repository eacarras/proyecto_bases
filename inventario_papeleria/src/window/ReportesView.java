package window;

import database.ConnectionDatabase;
import res.Values;

import reports.*;

import java.util.LinkedList;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.layout.AnchorPane;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;


public class ReportesView extends Application{
	
	// Root
	private AnchorPane root;
	
	// ComboBox options
	private ComboBox<String> cbreport_options;
	
	// Button for show the information and principal Label
	private Button btshow;
	private Label lbmessage;
	
	// Button for exit and values for return
	private Button exit;
	private String user;
	private boolean value;
	
	// Background
	private ImageView background;
	
	// LinkedList
	private LinkedList<String> report;
		
	public ReportesView(String user, boolean value) {
		// Get information
		this.user = user;
		this.value = value;
		
		// Background
		background = new ImageView(new Image("file:src/res/background-soft.jpg"));
		
		// Instance root 
		root = new AnchorPane();
		root.getChildren().add(background);
		
		// Button and Label
		manage_generic_information();
		
		// Create comboBox
		cbreport_options = new ComboBox<>();
		manageComboBox();
	}
	
	@Override
	public void start(Stage stage) {
		// Exit button
		exit.setOnAction(e -> {
			HomeActivity change_window = new HomeActivity(user, value);
			change_window.start(stage);
		});
		
		// Show button
		btshow.setOnAction(e -> {
			switch(cbreport_options.getValue()) {
			case "Cliente con mayor frecuencia de compra":
				report = ConnectionDatabase.obtenerReportes("cliente_frecuente");
				create_table("cliente_frecuente");
				break;
			case "Cantidad de Productos":
				report = ConnectionDatabase.obtenerReportes("productos_proveedor");
				System.out.println(report);
				create_table("productos_proveedor");
				break;
			case "Top 5 producto mas vendidos":
				report = ConnectionDatabase.obtenerReportes("producto_masven");
				create_table("producto_masven");
				break;
			case "Clientes y cantidad de compra":
				report = ConnectionDatabase.obtenerReportes("producto_cantidadcliente");
				create_table("producto_cantidadcliente");
				break;
			case "Producto con menor numero de ventas":
				report = ConnectionDatabase.obtenerReportes("producto_menosven");
				create_table("producto_menosven");
				break;
			case "Categoria con mayor productos vendidos":
				report = ConnectionDatabase.obtenerReportes("categoria_productoven");
				create_table("categoria_productoven");
				break;
			default:
				break;
			}
		});
		
		// Upload root
		Scene scene = new Scene(root, Values.WIDTH_WINDOW_REPORTS, Values.HEIGHT_WINDOW_REPORTS);
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.setResizable(false);
		stage.show();
	}
	
	@SuppressWarnings("unchecked")
	private void create_table(String view) {
		switch(view) {
		case "cliente_frecuente":
			TableView<ClienteMayorF> tabla = new TableView<>();
			AnchorPane.setTopAnchor(tabla, 50.0);
			AnchorPane.setLeftAnchor(tabla, 10.0);
			tabla.setEditable(false);
			tabla.setMaxWidth(800.0);
			tabla.setPrefWidth(500.0);
			
			TableColumn<ClienteMayorF, String> idcl = new TableColumn<>("Nombre");
	        TableColumn<ClienteMayorF, String> cedulaCliente = new TableColumn<>("Total");	     
	        
	        tabla.getColumns().addAll(idcl, cedulaCliente);
	        
	        idcl.setCellValueFactory(new PropertyValueFactory<>("nombre"));
	        cedulaCliente.setCellValueFactory(new PropertyValueFactory<>("total"));	       
	        
	        try {
				for(String value: report) {
					String[] array = value.split(",");					
					ClienteMayorF cliente = new ClienteMayorF(ConnectionDatabase.obtenerCliente(array[0]),
							array[1]);
					tabla.getItems().addAll(cliente);
				}
				
				this.root.getChildren().addAll(tabla);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "productos_proveedor":
			TableView<CantidadProd> tablap = new TableView<>();
			AnchorPane.setTopAnchor(tablap, 50.0);
			AnchorPane.setLeftAnchor(tablap, 10.0);
			tablap.setEditable(false);
			tablap.setMaxWidth(800.0);
			tablap.setPrefWidth(500.0);
			
			TableColumn<CantidadProd, String> nombre = new TableColumn<>("Nombre");
	        TableColumn<CantidadProd, String> stock = new TableColumn<>("Total");
	        TableColumn<CantidadProd, String> empresa = new TableColumn<>("Empresa");
	        
	        tablap.getColumns().addAll(nombre, stock, empresa);
	        
	        nombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
	        stock.setCellValueFactory(new PropertyValueFactory<>("stock"));
	        empresa.setCellValueFactory(new PropertyValueFactory<>("empresa"));
	        
	        try {
				for(String value: report) {
					String[] array = value.split(",");					
					CantidadProd cliente = new CantidadProd(array[0], array[1], array[2]);
					tablap.getItems().addAll(cliente);
				}
				
				this.root.getChildren().addAll(tablap);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "producto_masven":
			TableView<ProdMasVendidos> tablapmas = new TableView<>();
			AnchorPane.setTopAnchor(tablapmas, 50.0);
			AnchorPane.setLeftAnchor(tablapmas, 10.0);
			tablapmas.setEditable(false);
			tablapmas.setMaxWidth(800.0);
			tablapmas.setPrefWidth(500.0);
			
			TableColumn<ProdMasVendidos, String> nom = new TableColumn<>("Nombre");
	        TableColumn<ProdMasVendidos, String> tot = new TableColumn<>("Total");	     
	        
	        tablapmas.getColumns().addAll(nom, tot);
	        
	        nom.setCellValueFactory(new PropertyValueFactory<>("nombre"));
	        tot.setCellValueFactory(new PropertyValueFactory<>("total"));	       
	        
	        try {
				for(String value: report) {
					String[] array = value.split(",");					
					ProdMasVendidos cliente = new ProdMasVendidos(array[0], array[1]);
					tablapmas.getItems().addAll(cliente);
				}
				
				this.root.getChildren().addAll(tablapmas);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "producto_cantidadcliente":
			TableView<ClienteCompra> tablapcc = new TableView<>();
			AnchorPane.setTopAnchor(tablapcc, 50.0);
			AnchorPane.setLeftAnchor(tablapcc, 10.0);
			tablapcc.setEditable(false);
			tablapcc.setMaxWidth(800.0);
			tablapcc.setPrefWidth(500.0);
			
			TableColumn<ClienteCompra, String> nomn = new TableColumn<>("Nombre");
	        TableColumn<ClienteCompra, String> tota = new TableColumn<>("Total");	     
	        
	        tablapcc.getColumns().addAll(nomn, tota);
	        
	        nomn.setCellValueFactory(new PropertyValueFactory<>("nombre"));
	        tota.setCellValueFactory(new PropertyValueFactory<>("total"));	       
	        
	        try {
				for(String value: report) {
					String[] array = value.split(",");					
					ClienteCompra cliente = new ClienteCompra(array[0], array[1]);
					tablapcc.getItems().addAll(cliente);
				}
				
				this.root.getChildren().addAll(tablapcc);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "producto_menosven":
			TableView<ProdMenosVendidos> tablapmen = new TableView<>();
			AnchorPane.setTopAnchor(tablapmen, 50.0);
			AnchorPane.setLeftAnchor(tablapmen, 10.0);
			tablapmen.setEditable(false);
			tablapmen.setMaxWidth(800.0);
			tablapmen.setPrefWidth(500.0);
			
			TableColumn<ProdMenosVendidos, String> nomb = new TableColumn<>("Nombre");
	        TableColumn<ProdMenosVendidos, String> ttal = new TableColumn<>("Total");	     
	        
	        tablapmen.getColumns().addAll(nomb, ttal);
	        
	        nomb.setCellValueFactory(new PropertyValueFactory<>("nombre"));
	        ttal.setCellValueFactory(new PropertyValueFactory<>("total"));	       
	        
	        try {
				for(String value: report) {
					String[] array = value.split(",");					
					ProdMenosVendidos cliente = new ProdMenosVendidos(array[0], array[1]);
					tablapmen.getItems().addAll(cliente);
				}
				
				this.root.getChildren().addAll(tablapmen);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "categoria_productoven":
			TableView<CategoriaMasVendida> tablacp = new TableView<>();
			AnchorPane.setTopAnchor(tablacp, 50.0);
			AnchorPane.setLeftAnchor(tablacp, 10.0);
			tablacp.setEditable(false);
			tablacp.setMaxWidth(800.0);
			tablacp.setPrefWidth(500.0);
			
			TableColumn<CategoriaMasVendida, String> nmb = new TableColumn<>("Nombre");
	        TableColumn<CategoriaMasVendida, String> tal = new TableColumn<>("Total");	     
	        
	        tablacp.getColumns().addAll(nmb, tal);
	        
	        nmb.setCellValueFactory(new PropertyValueFactory<>("nombre"));
	        tal.setCellValueFactory(new PropertyValueFactory<>("total"));	       
	        
	        try {
				for(String value: report) {
					String[] array = value.split(",");					
					CategoriaMasVendida cliente = new CategoriaMasVendida(array[0], array[1]);
					tablacp.getItems().addAll(cliente);
				}
				
				this.root.getChildren().addAll(tablacp);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		}
	}
	
	private void manageComboBox() {
		cbreport_options.getItems().addAll(Values.array);
		AnchorPane.setLeftAnchor(cbreport_options, 10.0);
		AnchorPane.setTopAnchor(cbreport_options, 10.0);
		
		root.getChildren().add(cbreport_options);
	}
	
	private void manage_generic_information() {
		lbmessage = new Label("REPORTES");
		AnchorPane.setLeftAnchor(lbmessage, 10.0);
		AnchorPane.setTopAnchor(lbmessage, 10.0);
		
		btshow = new Button("Mostrar Reporte Seleccionado");
		AnchorPane.setRightAnchor(btshow, 10.0);
		AnchorPane.setTopAnchor(btshow, 10.0);
		
		exit = new Button("Regresar");
		AnchorPane.setRightAnchor(exit, 10.0);
		AnchorPane.setBottomAnchor(exit, 10.0);
		
		root.getChildren().addAll(lbmessage, btshow, exit);
	}
}
