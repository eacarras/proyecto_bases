package window;

import res.Values;

import database.ConnectionDatabase;

import java.util.LinkedList;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.event.ActionEvent;

public class PrincipalWindow extends Application{
	/*
	 * Pasos para crear un componente
	 * 1.- copias la estructura inicial *private .......*
	 * 2.- inicilizar ( = new Button())
	 * 3.- si es un boton, una accion*/
	// Linked User
	LinkedList<String> linked;
	
	// Principal Window
	private AnchorPane root;
	
	// ImageView for background
	private ImageView background;
	
	// Buttons
	private Button bt_login;

	// HBox
	private HBox husuario;
	private HBox hpass;
	
	// Labels and TextField
	// Label etiqueta
	private Label lb_usuario;
	// TEXTFIELD para obtener informacion
	private TextField user;
	private Label lb_pass;
	private PasswordField pass;
	
	// Constructor
	public PrincipalWindow() {
		// Linked
		linked = ConnectionDatabase.getUser();
		
		// Background
		background = new ImageView(new Image("file:src/res/background-login.jpg"));
		
		// Initialize all
		root = new AnchorPane();
		root.getChildren().add(background);
		
		husuario = new HBox();
		hpass = new HBox();
		
		lb_usuario = new Label("Usuario:      ");
		user = new TextField();
		lb_pass = new Label("Contrasena:");
		pass = new PasswordField();
	}
	
	
	// Cambiar pantalla si estas en la funcion main 'Nombreclase'.main(args);
	// Cambiar pantalla si estar en el start 
	/*
	 * 'Nombre' i = new 'Nombre'();
			i.start(stage);*/
	// Llamar a una funcion creada en la clase conexion de base de datos ConnectionDatabase.'nombre'();
	@Override
	public void start(Stage stage) {
		
		Button bt_prueba = new Button("Prueba");
		bt_prueba.setOnAction((ActionEvent e) -> {
			// String a = ConnectionDatabase.getUser();
			// Presentar informacion ` Label lb = new Label('le mandas el string');`
		});
		
		
		
		
		// HBox 
		husuario = manageBox(husuario, lb_usuario, user);
		hpass = manageBox(hpass, lb_pass, pass);
		
		AnchorPane.setLeftAnchor(husuario, 50.0);
		AnchorPane.setLeftAnchor(hpass, 50.0);
		
		AnchorPane.setTopAnchor(husuario, 100.0);
		AnchorPane.setTopAnchor(hpass, 160.0);
		
		// Set positions of the login button
		bt_login = new Button("Login");
		bt_login.setOnAction((ActionEvent e) -> validateCredentials(user.getText(),
				pass.getText(),stage));
		
		AnchorPane.setRightAnchor(bt_login, 20.0);
		AnchorPane.setBottomAnchor(bt_login, 20.0);
		
		// Add value to Root
		root.getChildren().addAll(husuario, hpass, bt_login);
		
		// Create scene
		Scene scene = new Scene(root, Values.getWIDTH_WINDOW(), 
				Values.getHEIGHT_WINDOW());
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.setTitle("Bienvenido");
		stage.setResizable(false);
		stage.show();
	}
	
	private HBox manageBox(HBox hbox, Label label, TextField textfield) {
		hbox.getChildren().addAll(label, textfield);
		hbox.setSpacing(Values.getSPACING_BUTTONS());
		return hbox;
	}
	
	private void validateCredentials(String user, String pass, Stage stage) {
		boolean isEquals = false;
		
		for (String credential: linked ) {
			String[] sp = credential.split(":");
			String person = sp[0];
			String password = sp[1];
			clean_nodes();
			if(person.equals(user) && password.equals(pass)) {
				isEquals = true;
				ConfirmationWindow confirmation = new ConfirmationWindow("Usuario y contrasena "
						+ "correctos ....", "AUTH", person, Boolean.valueOf(sp[2]));
				confirmation.start(stage);
			}
		}
		if(!isEquals) {
			ConfirmationWindow confirmation = new ConfirmationWindow("Usuario y contrasena "
					+ "incorrectos ....", "", null, null);
			confirmation.start(stage);
		}
	}
	
	private void clean_nodes() {
		this.user.clear();
		this.pass.clear();
	}
}