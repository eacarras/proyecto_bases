package window;

import res.Values;
import database.ConnectionDatabase;

import tdas.Categoria;
import tdas.Cliente;
import tdas.Compra;
import tdas.DetalleCompra;
import tdas.Direccion;
import tdas.Persona;
import tdas.Producto;
import tdas.Proveedor;
import tdas.Telefono;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;

import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

import javafx.scene.text.Font;

import java.util.LinkedList;
import java.util.ArrayList;

public class ModificationWindow extends Application{
	
	// Root, VBox
	private AnchorPane root;
	private VBox vbox_buttons;
	private String name_table;
	private String option;
	private LinkedList<String> linked;
	
	// Label
	private Label title;
	
	// Background
	private ImageView background;
	
	// Buttons
	private Button btadd;
	private Button btdrop;
	private Button btmodify;
	
	// Labels to add
	private VBox vinformation;
	private HBox h1;
	private Label l1;
	private TextField t1;
	private HBox h2;
	private Label l2;
	private TextField t2;
	private HBox h3;
	private Label l3;
	private TextField t3;
	private HBox h4;
	private Label l4;
	private TextField t4;
	private HBox h5;
	private Label l5;
	private TextField t5;
	private HBox h6;
	private Label l6;
	private TextField t6;
	private HBox h7;
	private Label l7;
	private TextField t7;
	private Button confirmn_button;
	
	// ComboBox to delete data
	private ArrayList<String> arrayCombo;
	private ComboBox<String> cbox;
	private VBox vdelete;
	private Button bdelete;
	
	// ComboBox to modified
	private HBox h1modified;
	private Label l1modified;
	private TextField t1modified;
	private HBox h2modified;
	private Label l2modified;
	private TextField t2modified;
	private HBox h3modified;
	private Label l3modified;
	private TextField t3modified;
	private HBox h4modified;
	private Label l4modified;
	private TextField t4modified;
	private HBox h5modified;
	private Label l5modified;
	private TextField t5modified;
	private HBox h6modified;
	private Label l6modified;
	private TextField t6modified;
	private ArrayList<String> arraymodified;
	private ComboBox<String> cboxmodified;
	private VBox vmodified;
	private Button bmodified;
	
	// Button to return to window
	private Button returnHome;
	private boolean bool;
	private String user;
	
	// Constructor
	public ModificationWindow(String name_table, boolean bool, String user) {
		// LinkedList and name Table
		linked = ConnectionDatabase.generalQuery(name_table);
		this.name_table = name_table;
		
		// Background
		background = new ImageView(new Image("file:src/res/background-modified.jpg"));
		
		// Initialize all
		root = new AnchorPane();
		root.getChildren().add(background);
		
		title = new Label(name_table.toUpperCase());
		
		btadd = new Button("Agregar Valor");
		btadd.setOnAction(e -> {
			this.option = "Add";
			vdelete.setVisible(false);
			vmodified.setVisible(false);
			vinformation.setVisible(true);
			setInformationLabels(name_table);
		});
		
		btmodify = new Button("Modificar valor existente");
		btmodify.setOnAction(e -> {
			this.option = "Modify";
			vdelete.setVisible(false);
			vinformation.setVisible(false);
			vmodified.setVisible(true);
			setInformationLabels(name_table);
		});
		
		btdrop = new Button("Eliminar valor existente");
		btdrop.setOnAction(e -> {
			this.option = "Drop";
			vinformation.setVisible(false);
			vmodified.setVisible(false);
			vdelete.setVisible(true);
		});
		setStyle();
		
		vbox_buttons = new VBox();
		addButtons();
		
		// Labels about add, remove o modified information
		createLabelsToInformation();
		manageVBoxLabelsInformation();
		
		cbox = new ComboBox<>();
		arrayCombo = new ArrayList<>();
		manageDeleteInfo();
		
		cboxmodified = new ComboBox<>();
		arraymodified = new ArrayList<>();
		createLabelsToModifiedInformation();
		manageVBoxLabelsModifiedInformation();
		
		this.bool = bool;
		this.user = user;
		returnHome = new Button("Regresar");
		AnchorPane.setBottomAnchor(returnHome, 20.0);
		AnchorPane.setRightAnchor(returnHome, 10.0);
	}
	
	@Override
	public void start(Stage stage) {
		// All the logic
		returnHome.setOnAction(e -> {
			HomeActivity home = new HomeActivity(user, bool);
			home.start(stage);
		});
		
		generatedTable();
		
		// Add nodes to the root
		root.getChildren().addAll(vbox_buttons, title, returnHome);
		
		Scene scene = new Scene(root, Values.WIDTH_WINDOW_HOME, Values.HEIGHT_WINDOW_HOME);
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.setResizable(false);
		stage.show();
	}
	
	private void addButtons() {
		vbox_buttons.getChildren().addAll(btadd, btmodify, btdrop);
		vbox_buttons.setSpacing(15.0);
		
		
		AnchorPane.setTopAnchor(vbox_buttons, 190.0);
		AnchorPane.setRightAnchor(vbox_buttons, 10.0);
	}
	
	private void setStyle() {
		btadd.setStyle("-fx-pref-width: 190px");
		btmodify.setStyle("-fx-pref-width: 190px");
		btdrop.setStyle("-fx-pref-width: 190px");
		
		title.setFont(new Font("Arial", 20));
		AnchorPane.setLeftAnchor(title, 5.0);
		AnchorPane.setTopAnchor(title, 5.0);
	}
	
	@SuppressWarnings("unchecked")
	private void generatedTable() {
		switch(this.name_table) {
		
		case "categoria":
			TableView<Categoria> tablacat = new TableView<>();
			AnchorPane.setTopAnchor(tablacat, 140.0);
			tablacat.setEditable(false);
			tablacat.setMaxWidth(240.0);
			tablacat.setPrefWidth(240.0);
			
			TableColumn<Categoria, String> id = new TableColumn<>("Id");
	        TableColumn<Categoria, String> nombre = new TableColumn<>("Nombre");
	        TableColumn<Categoria, String> descripcion = new TableColumn<>("Descripcion");
	        
	        tablacat.getColumns().addAll(id, nombre, descripcion);
	        
	        id.setCellValueFactory(new PropertyValueFactory<>("id"));
	        nombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
	        descripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
	        
	        try {
				for(String value: linked) {
					String[] array = value.split(":");
					arrayCombo.add(array[0]);
					arraymodified.add(array[0]);
					Categoria categoria = new Categoria(array[0], array[1], array[2]);
					tablacat.getItems().addAll(categoria);
				}
				this.cboxmodified.getItems().addAll(arraymodified);
				this.cbox.getItems().addAll(arrayCombo);
				this.root.getChildren().addAll(tablacat);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "cliente":
			TableView<Cliente> tablacl = new TableView<>();
			AnchorPane.setTopAnchor(tablacl, 140.0);
			tablacl.setEditable(false);
			tablacl.setMaxWidth(240.0);
			tablacl.setPrefWidth(240.0);
			
			TableColumn<Cliente, String> idcl = new TableColumn<>("Id");
	        TableColumn<Cliente, String> cedulaCliente = new TableColumn<>("Cedula Cliente");	     
	        
	        tablacl.getColumns().addAll(idcl, cedulaCliente);
	        
	        idcl.setCellValueFactory(new PropertyValueFactory<>("id"));
	        cedulaCliente.setCellValueFactory(new PropertyValueFactory<>("cedulaCliente"));	       
	        
	        try {
				for(String value: linked) {
					String[] array = value.split(":");
					arrayCombo.add(array[0]);
					arraymodified.add(array[0]);
					Cliente cliente = new Cliente(array[0], array[1]);
					tablacl.getItems().addAll(cliente);
				}
				this.cboxmodified.getItems().addAll(arraymodified);
				this.cbox.getItems().addAll(arrayCombo);
				this.root.getChildren().addAll(tablacl);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "compra":
			TableView<Compra> tablacom = new TableView<>();
			AnchorPane.setTopAnchor(tablacom, 140.0);
			tablacom.setEditable(false);
			tablacom.setMaxWidth(240.0);
			tablacom.setPrefWidth(240.0);
			
			TableColumn<Compra, String> idcom = new TableColumn<>("Id");
	        TableColumn<Compra, String> fecha = new TableColumn<>("Fecha");
	        TableColumn<Compra, String> montoFinal = new TableColumn<>("Monto Final");
	        TableColumn<Compra, String> impuesto = new TableColumn<>("Impuesto");
	        TableColumn<Compra, String> valorTotal = new TableColumn<>("Valor Total");
	        TableColumn<Compra, String> idCliente = new TableColumn<>("Id Cliente");
	        
	        tablacom.getColumns().addAll(idcom, fecha, montoFinal, impuesto, valorTotal, idCliente);
	        
	        idcom.setCellValueFactory(new PropertyValueFactory<>("id"));
	        fecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
	        montoFinal.setCellValueFactory(new PropertyValueFactory<>("montoFinal"));
	        impuesto.setCellValueFactory(new PropertyValueFactory<>("impuesto"));
	        valorTotal.setCellValueFactory(new PropertyValueFactory<>("valorTotal"));
	        idCliente.setCellValueFactory(new PropertyValueFactory<>("idCliente"));
	        
	        try {
				for(String value: linked) {
					String[] array = value.split(":");
					arrayCombo.add(array[0]);
					arraymodified.add(array[0]);
					Compra compra = new Compra(array[0], array[1], array[2], array[3], array[4], 
							ConnectionDatabase.obtenerCliente(array[5]));
					tablacom.getItems().addAll(compra);
				}
				this.cboxmodified.getItems().addAll(arraymodified);
				this.cbox.getItems().addAll(arrayCombo);
				this.root.getChildren().addAll(tablacom);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "detalle_compra":
			TableView<DetalleCompra> tabladcom = new TableView<>();
			AnchorPane.setTopAnchor(tabladcom, 140.0);
			tabladcom.setEditable(false);
			tabladcom.setMaxWidth(240.0);
			tabladcom.setPrefWidth(240.0);
			
			TableColumn<DetalleCompra, String> iddcom = new TableColumn<>("Id");
	        TableColumn<DetalleCompra, String> cantidad = new TableColumn<>("Cantidad");
	        TableColumn<DetalleCompra, String> valorUnitario = new TableColumn<>("Valor Unitario");
	        TableColumn<DetalleCompra, String> idCompra = new TableColumn<>("Id Compra");
	        TableColumn<DetalleCompra, String> idProducto = new TableColumn<>("Id Producto");
	        
	        tabladcom.getColumns().addAll(iddcom, cantidad, valorUnitario, idCompra, idProducto);
	        
	        iddcom.setCellValueFactory(new PropertyValueFactory<>("id"));
	        cantidad.setCellValueFactory(new PropertyValueFactory<>("cantidad"));
	        valorUnitario.setCellValueFactory(new PropertyValueFactory<>("valorUnitario"));
	        idCompra.setCellValueFactory(new PropertyValueFactory<>("idCompra"));
	        idProducto.setCellValueFactory(new PropertyValueFactory<>("idProducto"));	        
	        
	        try {
				for(String value: linked) {
					String[] array = value.split(":");
					arrayCombo.add(array[0]);
					arraymodified.add(array[0]);
					DetalleCompra detallecompra = new DetalleCompra(array[0], array[1], array[2], array[3],
							array[4]);
					tabladcom.getItems().addAll(detallecompra);
				}
				this.cboxmodified.getItems().addAll(arraymodified);
				this.cbox.getItems().addAll(arrayCombo);
				this.root.getChildren().addAll(tabladcom);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "direccion":
			TableView<Direccion> tabladir = new TableView<>();
			AnchorPane.setTopAnchor(tabladir, 140.0);
			tabladir.setEditable(false);
			tabladir.setMaxWidth(240.0);
			tabladir.setPrefWidth(240.0);
			
			TableColumn<Direccion, String> iddir = new TableColumn<>("Id");
	        TableColumn<Direccion, String> ciudad = new TableColumn<>("Ciudad");
	        TableColumn<Direccion, String> manzana = new TableColumn<>("Manzana");
	        TableColumn<Direccion, String> villa = new TableColumn<>("Villa");
	        TableColumn<Direccion, String> ciudadela = new TableColumn<>("Ciudadela");
	        TableColumn<Direccion, String> calle = new TableColumn<>("Calle");
	        TableColumn<Direccion, String> cdir = new TableColumn<>("Cedula");
	        
	        tabladir.getColumns().addAll(iddir, ciudad, manzana, villa, ciudadela, calle, cdir);
	        
	        iddir.setCellValueFactory(new PropertyValueFactory<>("id"));
	        ciudad.setCellValueFactory(new PropertyValueFactory<>("ciudad"));
	        manzana.setCellValueFactory(new PropertyValueFactory<>("manzana"));
	        villa.setCellValueFactory(new PropertyValueFactory<>("villa"));
	        ciudadela.setCellValueFactory(new PropertyValueFactory<>("ciudadela"));
	        calle.setCellValueFactory(new PropertyValueFactory<>("calle"));
	        cdir.setCellValueFactory(new PropertyValueFactory<>("cedula"));
	        
	        try {
				for(String value: linked) {
					String[] array = value.split(":");
					arrayCombo.add(array[0]);
					arraymodified.add(array[0]);
					Direccion direccion = new Direccion(array[0], array[1], array[2], array[3],
							array[4], array[5], array[6]);
					tabladir.getItems().addAll(direccion);
				}
				this.cboxmodified.getItems().addAll(arraymodified);
				this.cbox.getItems().addAll(arrayCombo);
				this.root.getChildren().addAll(tabladir);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "persona":
			TableView<Persona> tablaper = new TableView<>();
			AnchorPane.setTopAnchor(tablaper, 140.0);
			tablaper.setEditable(false);
			tablaper.setMaxWidth(240.0);
			tablaper.setPrefWidth(240.0);
			
			TableColumn<Persona, String> cedula = new TableColumn<>("Cedula");
	        TableColumn<Persona, String> name = new TableColumn<>("Nombre");
	        TableColumn<Persona, String> apellido = new TableColumn<>("Apellido");
	        
	        tablaper.getColumns().addAll(cedula, name, apellido);
	        
	        cedula.setCellValueFactory(new PropertyValueFactory<>("cedula"));
	        name.setCellValueFactory(new PropertyValueFactory<>("nombre"));
	        apellido.setCellValueFactory(new PropertyValueFactory<>("apellido"));
	        
	        try {
				for(String value: linked) {
					String[] array = value.split(":");
					arrayCombo.add(array[0]);
					arraymodified.add(array[0]);
					Persona persona = new Persona(array[0], array[1], array[2]);
					tablaper.getItems().addAll(persona);
				}
				this.cboxmodified.getItems().addAll(arraymodified);
				this.cbox.getItems().addAll(arrayCombo);
				this.root.getChildren().addAll(tablaper);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "producto":
			TableView<Producto> tablaprod = new TableView<>();
			AnchorPane.setTopAnchor(tablaprod, 140.0);
			tablaprod.setEditable(false);
			tablaprod.setMaxWidth(240.0);
			tablaprod.setPrefWidth(240.0);
			
			TableColumn<Producto, String> idprod = new TableColumn<>("Id");
	        TableColumn<Producto, String> n = new TableColumn<>("Nombre");
	        TableColumn<Producto, String> precio = new TableColumn<>("Precio");
	        TableColumn<Producto, String> stock = new TableColumn<>("Stock");
	        TableColumn<Producto, String> cProveedor = new TableColumn<>("Cedula Proveedor");
	        TableColumn<Producto, String> iCategoria = new TableColumn<>("Id Categoria");
	        
	        tablaprod.getColumns().addAll(idprod, n, precio, stock, cProveedor, iCategoria);
	        
	        idprod.setCellValueFactory(new PropertyValueFactory<>("id"));
	        n.setCellValueFactory(new PropertyValueFactory<>("nombre"));
	        precio.setCellValueFactory(new PropertyValueFactory<>("precio"));
	        stock.setCellValueFactory(new PropertyValueFactory<>("stock"));
	        cProveedor.setCellValueFactory(new PropertyValueFactory<>("cedulaProveedor"));
	        iCategoria.setCellValueFactory(new PropertyValueFactory<>("idCategoria"));
	        
	        try {
				for(String value: linked) {
					String[] array = value.split(":");
					arrayCombo.add(array[0]);
					arraymodified.add(array[0]);
					Producto producto = new Producto(array[0], array[1], array[2], array[3],
							array[4], ConnectionDatabase.obtenerCategoria(array[5]));
					tablaprod.getItems().addAll(producto);
				}
				this.cboxmodified.getItems().addAll(arraymodified);
				this.cbox.getItems().addAll(arrayCombo);
				this.root.getChildren().addAll(tablaprod);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "proveedor":
			TableView<Proveedor> tablapro = new TableView<>();
			AnchorPane.setTopAnchor(tablapro, 140.0);
			tablapro.setEditable(false);
			tablapro.setMaxWidth(240.0);
			tablapro.setPrefWidth(240.0);
			
			TableColumn<Proveedor, String> cedulaProveedor = new TableColumn<>("Cedula Proveedor");
	        TableColumn<Proveedor, String> empresa = new TableColumn<>("Empresa");	      
	        
	        tablapro.getColumns().addAll(cedulaProveedor, empresa);
	        
	        cedulaProveedor.setCellValueFactory(new PropertyValueFactory<>("cedulaProveedor"));
	        empresa.setCellValueFactory(new PropertyValueFactory<>("empresa"));	        
	        
	        try {
				for(String value: linked) {
					String[] array = value.split(":");
					arrayCombo.add(array[0]);
					arraymodified.add(array[0]);
					Proveedor proveedor = new Proveedor(array[0], array[1]);
					tablapro.getItems().addAll(proveedor);
				}
				this.cboxmodified.getItems().addAll(arraymodified);
				this.cbox.getItems().addAll(arrayCombo);
				this.root.getChildren().addAll(tablapro);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
			
		case "telefono":
			TableView<Telefono> tablatel = new TableView<>();
			AnchorPane.setTopAnchor(tablatel, 140.0);
			tablatel.setEditable(false);
			tablatel.setMaxWidth(240.0);
			tablatel.setPrefWidth(240.0);
			
			TableColumn<Telefono, String> numeroTelefono = new TableColumn<>("Numero Telefono");
	        TableColumn<Telefono, String> cedulatel = new TableColumn<>("Cedula");	      
	        
	        tablatel.getColumns().addAll(numeroTelefono, cedulatel);
	        
	        numeroTelefono.setCellValueFactory(new PropertyValueFactory<>("numeroTelefono"));
	        cedulatel.setCellValueFactory(new PropertyValueFactory<>("cedula"));	        
	        
	        try {
				for(String value: linked) {
					String[] array = value.split(":");
					arrayCombo.add(array[0]);
					arraymodified.add(array[0]);
					Telefono telefono = new Telefono(array[0], array[1]);
					tablatel.getItems().addAll(telefono);
				}
				this.cboxmodified.getItems().addAll(arraymodified);
				this.cbox.getItems().addAll(arrayCombo);
				this.root.getChildren().addAll(tablatel);
			}catch(Exception e) {
				System.out.println("Errores al generar la tabla");
			}
			break;
		}
	}
	
	private void createLabelsToInformation() {
		l1 = new Label();
		t1 = new TextField();
		h1 = new HBox();
		h1.getChildren().addAll(l1, t1);
		h1.setSpacing(10.0);
		
		l2 = new Label();
		t2 = new TextField();
		h2 = new HBox();
		h2.getChildren().addAll(l2, t2);
		h2.setSpacing(10.0);
		
		l3 = new Label();
		t3 = new TextField();
		h3 = new HBox();
		h3.getChildren().addAll(l3, t3);
		h3.setSpacing(10.0);
		
		l4 = new Label();
		t4 = new TextField();
		h4 = new HBox();
		h4.getChildren().addAll(l4, t4);
		h4.setSpacing(10.0);
		
		l5 = new Label();
		t5 = new TextField();
		h5 = new HBox();
		h5.getChildren().addAll(l5, t5);
		h5.setSpacing(10.0);
		
		l6 = new Label();
		t6 = new TextField();
		h6 = new HBox();
		h6.getChildren().addAll(l6, t6);
		h6.setSpacing(10.0);
		
		l7 = new Label();
		t7 = new TextField();
		h7 = new HBox();
		h7.getChildren().addAll(l7, t7);
		h7.setSpacing(10.0);
		
		confirmn_button = new Button("Confirmar");
		confirmn_button.setOnAction(e -> {
			buttonOption(this.option);
			cleanNodes();
		});
	}
	
	private void createLabelsToModifiedInformation() {
		h1modified = new HBox();
		l1modified = new Label();
		t1modified = new TextField();
		h1modified.getChildren().addAll(l1modified, t1modified);
		h1modified.setSpacing(10.0);
		
		h2modified = new HBox();
		l2modified = new Label();
		t2modified = new TextField();
		h2modified.getChildren().addAll(l2modified, t2modified);
		h2modified.setSpacing(10.0);
		
		h3modified = new HBox();
		l3modified = new Label();
		t3modified = new TextField();
		h3modified.getChildren().addAll(l3modified, t3modified);
		h3modified.setSpacing(10.0);
		
		h4modified = new HBox();
		l4modified = new Label();
		t4modified = new TextField();
		h4modified.getChildren().addAll(l4modified, t4modified);
		h4modified.setSpacing(10.0);
		
		h5modified = new HBox();
		l5modified = new Label();
		t5modified = new TextField();
		h5modified.getChildren().addAll(l5modified, t5modified);
		h5modified.setSpacing(10.0);
		
		h6modified = new HBox();
		l6modified = new Label();
		t6modified = new TextField();
		h6modified.getChildren().addAll(l6modified, t6modified);
		h6modified.setSpacing(10.0);
		
		bmodified = new Button("Modificar");
		bmodified.setOnAction(e -> {
			buttonOption(this.option);
			cleanNodes();
		});
	}
	
	private void manageVBoxLabelsInformation() {
		vinformation = new VBox();
		vinformation.setSpacing(5.0);
		vinformation.setVisible(false);
		
		vinformation.getChildren().addAll(h1, h2, h3, h4, h5, h6, h7, confirmn_button);
		AnchorPane.setLeftAnchor(vinformation, 250.0);
		AnchorPane.setTopAnchor(vinformation, 140.0);
		
		root.getChildren().addAll(vinformation);
	}
	
	private void manageVBoxLabelsModifiedInformation() {
		vmodified = new VBox();
		vmodified.setSpacing(5.0);
		vmodified.setVisible(false);
		
		vmodified.getChildren().addAll(cboxmodified, h1modified, h2modified, h3modified, h4modified, h5modified,
				h6modified, bmodified);
		AnchorPane.setLeftAnchor(vmodified, 250.0);
		AnchorPane.setTopAnchor(vmodified, 140.0);
		
		root.getChildren().addAll(vmodified);
	}
	
	private void manageDeleteInfo() {
		bdelete = new Button("Eliminar");
		bdelete.setOnAction(e -> {
			buttonOption(this.option);
		});
		
		vdelete = new VBox();
		vdelete.setSpacing(10.0);
		vdelete.setVisible(false);
		
		vdelete.getChildren().addAll(cbox, bdelete);
		AnchorPane.setLeftAnchor(vdelete, 250.0);
		AnchorPane.setTopAnchor(vdelete, 140.0);
		
		root.getChildren().add(vdelete);
	}
	
	private void setInformationLabels(String nameTable) {
		switch(this.name_table) {
		case "categoria":
			l1.setText("Id");
			l1modified.setText("Nombre");
			l2.setText("Nombre");
			l2modified.setText("Descripcion");
			l3.setText("Descripcion");
			t3modified.setVisible(false);
			t4.setVisible(false);
			t4modified.setVisible(false);
			t5.setVisible(false);
			t5modified.setVisible(false);
			t6.setVisible(false);
			t6modified.setVisible(false);
			t7.setVisible(false);
			break;
			
		case "cliente":
			l1.setText("Id");
			l1modified.setText("Cedula");
			l2.setText("Cedula");
			t2modified.setVisible(false);
			t3.setVisible(false);
			t3modified.setVisible(false);
			t4.setVisible(false);
			t4modified.setVisible(false);
			t5.setVisible(false);
			t5modified.setVisible(false);
			t6.setVisible(false);
			t6modified.setVisible(false);
			t7.setVisible(false);
			break;
			
		case "compra":
			l1.setText("Id");
			l1modified.setText("Fecha");
			l2.setText("Fecha");
			l2modified.setText("Monto");
			l3.setText("Monto");
			l3modified.setText("Impuesto");
			l4.setText("Impuesto");
			l4modified.setText("Total");
			l5.setText("Total");
			l5modified.setText("Id Cliente");
			l6.setText("Id Cliente");
			t6modified.setVisible(false);
			t7.setVisible(false);
			break;
			
		case "detalle_compra":
			l1.setText("Id");
			l1modified.setText("Cantidad");
			l2.setText("Cantidad");
			l2modified.setText("Valor");
			l3.setText("Valor");
			l3modified.setText("Id Compra");
			l4.setText("Id Compra");
			l4modified.setText("Id Producto");
			l5.setText("Id Producto");
			t5modified.setVisible(false);
			t6.setVisible(false);
			t6modified.setVisible(false);
			t7.setVisible(false);
			break;
			
		case "direccion":
			l1.setText("Id");
			l1modified.setText("Ciudad");
			l2.setText("Ciudad");
			l2modified.setText("Manzana");
			l3.setText("Manzana");
			l3modified.setText("Villa");
			l4.setText("Villa");
			l4modified.setText("Ciudadela");
			l5.setText("Ciudadela");
			l5modified.setText("Calle");
			l6.setText("Calle");
			l6modified.setText("Cedula");
			l7.setText("Cedula");
			break;
			
		case "persona":
			l1.setText("Cedula");
			l1modified.setText("Nombre");
			l2.setText("Nombre");
			l2modified.setText("Apellido");
			l3.setText("Apellido");
			t3modified.setVisible(false);
			t4.setVisible(false);
			t4modified.setVisible(false);
			t5.setVisible(false);
			t5modified.setVisible(false);
			t6.setVisible(false);
			t6modified.setVisible(false);
			t7.setVisible(false);
			break;
			
		case "producto":
			l1.setText("Id");
			l1modified.setText("Nombre");
			l2.setText("Nombre");
			l2modified.setText("Precio");
			l3.setText("Precio");
			l3modified.setText("Stock");
			l4.setText("Stock");
			l4modified.setText("Cedula");
			l5.setText("Cedula");
			l5modified.setText("Categoria");
			l6.setText("Categoria");
			t6modified.setVisible(false);
			t7.setVisible(false);
			break;
			
		case "proveedor":
			l1.setText("Cedula");
			l1modified.setText("Empresa");
			l2.setText("Empresa");
			t2modified.setVisible(false);
			t3.setVisible(false);
			t3modified.setVisible(false);
			t4.setVisible(false);
			t4modified.setVisible(false);
			t5.setVisible(false);
			t5modified.setVisible(false);
			t6.setVisible(false);
			t6modified.setVisible(false);
			t7.setVisible(false);
			break;
			
		case "telefono":
			l1.setText("Numero");
			l1modified.setText("Cedula");
			l2.setText("Cedula");
			t2modified.setVisible(false);
			t3.setVisible(false);
			t3modified.setVisible(false);
			t4.setVisible(false);
			t4modified.setVisible(false);
			t5.setVisible(false);
			t5modified.setVisible(false);
			t6.setVisible(false);
			t6modified.setVisible(false);
			t7.setVisible(false);
			break;
		}
	}
	
	// Only for add and drop 
	private void buttonOption(String action) {
		String data = null;
		String condition = null;
		
		if(action.equals("Add")) {
			switch(name_table) {
			case "categoria":
				data = t1.getText() + ", \'" + t2.getText() + "\', \'" + t3.getText() + "\'"; 
				ConnectionDatabase.insertData(name_table, data);
				break;
				
			case "cliente":
				data = t1.getText() + ", " + t2.getText();
				ConnectionDatabase.insertData(name_table, data);
				break;
				
			case "compra":
				data = t1.getText() + ", now(), " + t3.getText() + ", " + t4.getText() + ", " + t5.getText() + ", " + t6.getText();
				ConnectionDatabase.insertData(name_table, data);
				break;
				
			case "detalle_compra":
				data = t1.getText() + ", " + t2.getText() + ", " + t3.getText() + ", " + t4.getText() + ", " + t5.getText();
				ConnectionDatabase.insertData(name_table, data);
				break;
				
			case "direccion":
				data = t1.getText() + ", \'" + t2.getText() + "\', \'" + t3.getText() + "\', \'" + t4.getText() + "\', \'" +
						t5.getText() + "\', \'" + t6.getText() + "\', " + t7.getText();
				ConnectionDatabase.insertData(name_table, data);
				break;
				
			case "persona":
				data = t1.getText() + ", \'" + t2.getText() + "\', \'" + t3.getText() + "\'";
				ConnectionDatabase.insertData(name_table, data);
				break;
				
			case "producto":
				data = t1.getText() + ", \'" + t2.getText() + "\', " + t3.getText() + ", " + t4.getText() + ", " + 
						t5.getText() + ", " + t6.getText();
				ConnectionDatabase.insertData(name_table, data);
				break;
				
			case "proveedor":
				data = t1.getText() + ", \'" + t2.getText() + "\'";
				ConnectionDatabase.insertData(name_table, data);
				break;
				
			case "telefono":
				data = t1.getText() + ", " + t2.getText();
				ConnectionDatabase.insertData(name_table, data);
				break;
			}
		} else if(action.equals("Drop")) {
			switch(name_table) {
			case "categoria":
				condition = "id = " + cbox.getValue();
				ConnectionDatabase.deleteData(name_table, condition);
				break;
				
			case "cliente":
				condition = "id = " + cbox.getValue();
				ConnectionDatabase.deleteData(name_table, condition);
				break;
				
			case "compra":
				condition = "id = " + cbox.getValue();
				ConnectionDatabase.deleteData(name_table, condition);
				break;
				
			case "detalle_compra":
				condition = "id = " + cbox.getValue();
				ConnectionDatabase.deleteData(name_table, condition);
				break;
				
			case "direccion":
				condition = "id = " + cbox.getValue();
				ConnectionDatabase.deleteData(name_table, condition);
				break;
				
			case "persona":
				condition = "cedula = " + cbox.getValue();
				ConnectionDatabase.deleteData(name_table, condition);
				break;
				
			case "producto":
				condition = "id = " + cbox.getValue();
				ConnectionDatabase.deleteData(name_table, condition);
				break;
				
			case "proveedor":
				condition = "cedulaProveedor = " + cbox.getValue();
				ConnectionDatabase.deleteData(name_table, condition);
				break;
				
			case "telefono":
				condition = "numeroTelefono = " + cbox.getValue();
				ConnectionDatabase.deleteData(name_table, condition);
				break;
			}
		} else {
			switch(name_table) {
			case "categoria":
				condition = "id = " + cboxmodified.getValue();
				data = "nombre = \'" + t1modified.getText() + "\', descripcion = \'" + t2modified.getText() + "\'";
				ConnectionDatabase.actualizarRegistro(name_table, data, condition);
				break;
				
			case "cliente":
				condition = "id = " + cboxmodified.getValue();
				data = "cedulaCliente = " + t1modified.getText();
				ConnectionDatabase.actualizarRegistro(name_table, data, condition);
				break;
				
			case "compra":
				condition = "id = " + cboxmodified.getValue();
				data = "fecha = now(), montoFinal = " + t2modified.getText() + ", impuesto = " + t3modified.getText() + 
						", valorTotal = " + t4modified.getText() + ", idCliente = " + t5modified.getText();
				ConnectionDatabase.actualizarRegistro(name_table, data, condition);
				break;
				
			case "detalle_compra":
				condition = "id = " + cboxmodified.getValue();
				data = "cantidad = " + t1modified.getText() + ", valorUnitario = " + t2modified.getText() + ", idCompra = " + 
						t3modified.getText() + ", idProducto = " + t4modified.getText();
				ConnectionDatabase.actualizarRegistro(name_table, data, condition);
				break;
				
			case "direccion":
				condition = "id = " + cboxmodified.getValue();
				data = "ciudad = \'" + t1modified.getText() + "\', manzana = \'" + t2modified.getText() + "\', villa = \'" + 
						t3modified.getText() + "\', ciudadela = \'" + t4modified.getText() + "\', calle = \'" + t5modified.getText() + 
						"\', cedula = " + t6modified.getText();
				ConnectionDatabase.actualizarRegistro(name_table, data, condition);
				break;
				
			case "persona":
				condition = "cedula = " + cboxmodified.getValue();
				data = "nombre = \'" + t1modified.getText() + "\', apellido = \'" + t2modified.getText() + "\'";
				ConnectionDatabase.actualizarRegistro(name_table, data, condition);
				break;
				
			case "producto":
				condition = "cedula = " + cboxmodified.getValue();
				data = "nombre = \'" + t1modified.getText() + "\', precio = " + t2modified.getText() + ", stock = " + t3modified.getText() + 
						", cedulaProveedor = " + t4modified.getText() + ", idCategoria = " + t5modified.getText();
				ConnectionDatabase.actualizarRegistro(name_table, data, condition);
				break;
				
			case "proveedor":
				condition = "cedulaProveedor = " + cboxmodified.getValue();
				data = "empresa = \'" + t1modified.getText() + "\'";
				ConnectionDatabase.actualizarRegistro(name_table, data, condition);
				break;
				
			case "telefono":
				condition = "numeroTelefono = " + cboxmodified.getValue();
				data = "cedula = " + t1modified.getText();
				ConnectionDatabase.actualizarRegistro(name_table, data, condition);
				break;
			}
		}
	}
	
	private void cleanNodes() {
		t1.setText("");
		t2.setText("");
		t3.setText("");
		t4.setText("");
		t5.setText("");
		t6.setText("");
		t7.setText("");
		
		t1modified.setText("");
		t2modified.setText("");
		t3modified.setText("");
		t4modified.setText("");
		t5modified.setText("");
		t6modified.setText("");
	}
 }