package database;

import java.sql.*;

import java.util.Properties;
import java.util.LinkedList;

public class ConnectionDatabase {

	// Library, NameOfDatabase, Host and Port
	public static String driver = "com.mysql.cj.jdbc.Driver";
	public static String database = "papeleria";
	public static String hostname = "localhost";

	// Path
	public static String url = "jdbc:mysql://" + hostname + "/" + database;
	
	// Realiza la conexion a la base de datos
	private static Connection conectarMySQL() {
		Connection conn = null;
		Properties proper = new Properties();
		proper.put("user", "userp");
		proper.put("password", "Userp@092");

		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, proper);
		} catch (Exception e) {
			System.out.println("El problema con la base de datos es: " + e);
		}

		return conn;
	}

	public static LinkedList<String> generalQuery(String nameTable) {
		String query = "SELECT * FROM " + nameTable + ";";
		LinkedList<String> linked = new LinkedList<>();

		Connection db = null;
		Statement st = null;
		ResultSet rs = null;

		try {
			db = conectarMySQL();
			st = db.createStatement();
			rs = st.executeQuery(query);

			switch (nameTable) {
			case "categoria":
				while (rs.next()) {
					String value = String.valueOf(rs.getInt("id")) + ":" + rs.getString("nombre") + ":"
							+ rs.getString("descripcion");
					linked.add(value);
				}
				break;
			case "cliente":
				while (rs.next()) {
					String value = String.valueOf(rs.getInt("id")) + ":" + rs.getString("cedulaCliente");
					linked.add(value);
				}
				break;
			case "compra":
				while (rs.next()) {
					String value = String.valueOf(rs.getInt("id")) + ":" + rs.getDate("fecha").toString() + ":" +
							String.valueOf(rs.getFloat("montoFinal")) + ":" + String.valueOf(rs.getFloat("impuesto")) + 
							":" + String.valueOf(rs.getFloat("valorTotal")) + ":" + 
							String.valueOf(rs.getInt("idCliente"));
					linked.add(value);
				}
				break;
			case "detalle_compra":
				while (rs.next()) {
					String value = String.valueOf(rs.getInt("id")) + ":" + String.valueOf(rs.getInt("cantidad")) + ":" +
							String.valueOf(rs.getFloat("valorUnitario")) + ":" + String.valueOf(rs.getInt("idCompra")) + 
							":" + String.valueOf(rs.getInt("idProducto"));
					linked.add(value);
				}
				break;
			case "direccion":
				while (rs.next()) {
					String value = String.valueOf(rs.getInt("id")) + ":" + rs.getString("ciudad") + ":" +
							rs.getString("manzana") + ":" + rs.getString("villa") + ":" + 
							rs.getString("ciudadela") + ":" + rs.getString("calle") + ":" + 
							String.valueOf(rs.getInt("cedula"));
					linked.add(value);
				}
				break;
			case "persona":
				while (rs.next()) {
					String value = String.valueOf(rs.getInt("cedula")) + ":" + rs.getString("nombre") + 
							":" + rs.getString("apellido");
					linked.add(value);
				}
				break;
			case "producto":
				while (rs.next()) {
					String value = String.valueOf(rs.getInt("id")) + ":" + rs.getString("nombre") + 
							":" + String.valueOf(rs.getFloat("precio")) + ":" + String.valueOf(rs.getInt("stock")) + 
							":" + String.valueOf(rs.getInt("cedulaProveedor")) + ":" + 
							String.valueOf(rs.getInt("idCategoria"));
					linked.add(value);
				}
				break;
			case "proveedor":
				while(rs.next()) {
					String value = String.valueOf(rs.getInt("cedulaProveedor")) + ":" + 
							rs.getString("empresa");
					linked.add(value);
				}
				break;
			case "telefono":
				while(rs.next()) {
					String value = String.valueOf(rs.getInt("numeroTelefono")) + ":" + 
							String.valueOf(rs.getInt("cedula"));
					linked.add(value);
				}
				break;
			default:
				break;
			}
		} catch (Exception e) {
			System.out.println("Problems in the Query, " + e);
		}
		return linked;
	}
	
	// public static 'tipo de dato' nombre() retornar 
	// declarar una variable 'tipo de datos' 'nombre'
	public static LinkedList<String> getUser() {
		String query = "SELECT * FROM usuario";
		LinkedList<String> linked = new LinkedList<>();

		Connection db = null;
		Statement st = null;
		ResultSet rs = null;

		try {
			db = conectarMySQL();
			st = db.createStatement();
			// Te bota los resultados del query
			rs = st.executeQuery(query);

			while (rs.next()) { 
				String value = rs.getString("nombre") + ":"+ rs.getString("contrasena") + ":" + 
						String.valueOf(rs.getBoolean("root"));
				linked.add(value);
			}
		} catch (Exception e) {
			System.out.println("Problems in the Query, " + e);
		}

		return linked;
	}
	
	// User need to send information in the way user,password,root
	@SuppressWarnings("unused")
	public static boolean addUser(String user) {
		String query = "INSERT INTO usuario(id, nombre, contrasena, root) VALUES (" + idNewUser() + ", " + user + ");";

		Connection db = null;
		Statement st = null;
		
		try {
			db = conectarMySQL();
			st = db.createStatement();
			System.out.println(query);
			if(st.execute(query)) System.out.println("Se agrego correctamente");
			else System.out.println("Problemas al ingresar la data");
		} catch (Exception e) {
			System.out.println("Problems in the Query, " + e);
			return false;
		}

		return true;
	}
	
	public static boolean insertData(String nameTable, String data) {
		String query = "INSERT INTO " + nameTable + " VALUES (" + data + ");";

		Connection db = null;
		Statement st = null;
		
		try {
			db = conectarMySQL();
			st = db.createStatement();
			System.out.println(query);
			if(st.execute(query)) System.out.println("Se agrego correctamente");
			else System.out.println("Problemas al ingresar la data");
		} catch (Exception e) {
			System.out.println("Problems in the Query, " + e);
			return false;
		}
		
		return true;
	}
	
	public static boolean deleteData(String nameTable, String condition) {
		String query = "DELETE FROM " + nameTable + " WHERE " + condition + ";";

		Connection db = null;
		Statement st = null;
		
		try {
			db = conectarMySQL();
			st = db.createStatement();
			System.out.println(query);
			if(st.execute(query)) System.out.println("Se elimino correctamente");
			else System.out.println("Problemas al eliminar la data");
		} catch (Exception e) {
			System.out.println("Problems in the Query, " + e);
			return false;
		}
		
		return true;
	}
	
	public static boolean actualizarRegistro(String nameTable, String data, String condition) {
		String query = "UPDATE " + nameTable + " SET " + data + " WHERE " + condition + ";";

		Connection db = null;
		Statement st = null;
		
		try {
			db = conectarMySQL();
			st = db.createStatement();
			System.out.println(query);
			if(st.execute(query)) System.out.println("Se actualizo correctamente");
			else System.out.println("Problemas al actualizar la data");
		} catch (Exception e) {
			System.out.println("Problems in the Query, " + e);
			return false;
		}
		
		return true;
	}
	
	private static String idNewUser() {
		String query = "SELECT * FROM usuario";
		String id = null;

		Connection db = null;
		Statement st = null;
		ResultSet rs = null;

		try {
			db = conectarMySQL();
			st = db.createStatement();
			rs = st.executeQuery(query);

			while (rs.next()) {
				id = String.valueOf(rs.getInt("id") + 1);
			}
		} catch (Exception e) {
			System.out.println("Problems in the Query, " + e);
		}
		
		return id;
	}
	
	public static String obtenerCategoria(String idcat) {
		String query = "SELECT * FROM categoria;";
		String category = null;
		
		Connection db = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			db = conectarMySQL();
			st = db.createStatement();
			rs = st.executeQuery(query);

			while (rs.next()) {
				String id = String.valueOf(rs.getInt("id"));
				if(id.equals(idcat)) {
					category = rs.getString("nombre");
					return category;
				}
			}
		} catch (Exception e) {
			System.out.println("Problems in the Query, " + e);
		}
		
		return category;
	}
	
	public static String obtenerCliente(String idcat) {
		String query = "SELECT * FROM cliente;";
		String cedula = null;
		
		Connection db = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			db = conectarMySQL();
			st = db.createStatement();
			rs = st.executeQuery(query);

			while (rs.next()) {
				String id = String.valueOf(rs.getInt("id"));
				if(id.equals(idcat)) {
					cedula = rs.getString("cedulaCliente");
					return cedula;
				}
			}
		} catch (Exception e) {
			System.out.println("Problems in the Query, " + e);
		}
		
		return cedula;
	}
	
	// TODO
	public static LinkedList<String> obtenerReportes(String name_view) {
		String query = "SELECT * FROM " + name_view + ";";
		LinkedList<String> result = new LinkedList<>();
		
		Connection db = null;
		Statement st = null;
		ResultSet rs = null;
		
		try {
			db = conectarMySQL();
			st = db.createStatement();
			rs = st.executeQuery(query);
			while(rs.next()) {
				switch(name_view) {
				case "cliente_frecuente":
					result.add(String.valueOf(rs.getInt("idCliente")) + "," + String.valueOf(rs.getInt("total")));
					break;
				case "productos_proveedor":
					result.add(rs.getString("nombre") + "," + String.valueOf(rs.getInt("stock")) + "," + 
							rs.getString("empresa"));
					break;
				case "producto_masven":
					result.add(rs.getString("nombre") + "," + String.valueOf(rs.getInt("total")));
					break;
				case "producto_cantidadcliente":
					result.add(rs.getString("nombre") + "," + String.valueOf(rs.getInt("total")));
					break;
				case "producto_menosven":
					result.add(rs.getString("nombre") + "," + String.valueOf(rs.getInt("total")));
					break;
				case "categoria_productoven":
					result.add(rs.getString("nombre") + "," + String.valueOf(rs.getInt("total")));
					break;
				}
			}
		} catch(Exception e) {
			System.out.println("Problems getting the view");
		}
		return result;
	}
}
