package tdas;

public class Proveedor {
	
	// Attributes
	private String cedulaProveedor;
	private String empresa;
	
	// Constructor
	public Proveedor(String cedulaProveedor, String empresa) {
		super();
		this.cedulaProveedor = cedulaProveedor;
		this.empresa = empresa;
	}
	
	// Getter and Setter
	public String getCedulaProveedor() {
		return cedulaProveedor;
	}

	public void setCedulaProveedor(String cedulaProveedor) {
		this.cedulaProveedor = cedulaProveedor;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
}
