package tdas;

public class Telefono {
	
	private String numeroTelefono;
	private String cedula;
	
	public Telefono(String numeroTelefono, String cedula) {
		this.numeroTelefono = numeroTelefono;
		this.cedula = cedula;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
}
