package tdas;

public class Compra {
	
	// Attributes
	private String id;
	private String fecha;
	private String montoFinal;
	private String impuesto;
	private String valorTotal;
	private String idCliente;
	
	// Constructor
	public Compra(String id, String fecha, String montoFinal, String impuesto, String valorTotal, String idCliente) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.montoFinal = montoFinal;
		this.impuesto = impuesto;
		this.valorTotal = valorTotal;
		this.idCliente = idCliente;
	}
	
	// Getter and Setter
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getMontoFinal() {
		return montoFinal;
	}

	public void setMontoFinal(String montoFinal) {
		this.montoFinal = montoFinal;
	}

	public String getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}

	public String getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
}
