package tdas;

public class Cliente {
	
	// Attributes
	private String id;
	private String cedulaCliente;
	
	// Constructor
	public Cliente(String id, String cedulaCliente) {
		this.id = id;
		this.cedulaCliente = cedulaCliente;
	}
	
	// Getters and Setters
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCedulaCliente() {
		return cedulaCliente;
	}

	public void setCedulaCliente(String cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}
}
