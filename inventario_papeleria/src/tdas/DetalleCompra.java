package tdas;

public class DetalleCompra {
	
	// Attributes
	private String id;
	private String cantidad;
	private String valorUnitario;
	private String idCompra;
	private String idProducto;
	
	// Constructor
	public DetalleCompra(String id, String cantidad, String valorUnitario, String idC, String idProducto) {
		this.id = id;
		this.cantidad = cantidad;
		this.valorUnitario = valorUnitario;
		this.idCompra = idC;
		this.idProducto = idProducto;
	}
	
	// Getter and Setter
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(String valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public String getIdCompra() {
		return idCompra;
	}

	public void setIdCompra(String idCompra) {
		this.idCompra = idCompra;
	}

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
}
