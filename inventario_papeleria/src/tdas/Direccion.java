package tdas;

public class Direccion {
	
	// Attributes
	private String id;
	private String ciudad;
	private String manzana;
	private String villa;
	private String ciudadela;
	private String calle;
	private String cedula;
	
	// Constructor
	public Direccion(String id, String ciudad, String manzana, String villa, String ciudadela, String calle, String cedula) {
		this.id = id;
		this.ciudad = ciudad;
		this.manzana = manzana;
		this.villa = villa;
		this.ciudadela = ciudadela;
		this.calle = calle;
		this.cedula = cedula;
	}

	// Getter and Setter
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getManzana() {
		return manzana;
	}

	public void setManzana(String manzana) {
		this.manzana = manzana;
	}

	public String getVilla() {
		return villa;
	}

	public void setVilla(String villa) {
		this.villa = villa;
	}

	public String getCiudadela() {
		return ciudadela;
	}

	public void setCiudadela(String ciudadela) {
		this.ciudadela = ciudadela;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
}
