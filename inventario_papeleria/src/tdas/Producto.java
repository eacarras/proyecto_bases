package tdas;

public class Producto {
	
	// Attributes
	private String id;
	private String nombre;
	private String precio;
	private String stock;
	private String cedulaProveedor;
	private String idCategoria;
	
	// Constructor
	public Producto(String id, String nombre, String precio, String stock, String cedulaProveedor, String idCategoria) {
		this.id = id;
		this.nombre = nombre;
		this.precio = precio;
		this.stock = stock;
		this.cedulaProveedor = cedulaProveedor;
		this.idCategoria = idCategoria;
	}

	// Getter and Setter
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNomnbre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getCedulaProveedor() {
		return cedulaProveedor;
	}

	public void setCedulaProveedor(String cedulaProveedor) {
		this.cedulaProveedor = cedulaProveedor;
	}

	public String getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(String idCategoria) {
		this.idCategoria = idCategoria;
	}
}
