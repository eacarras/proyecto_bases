CREATE DATABASE papeleria;
USE papeleria;
CREATE TABLE persona(
	cedula INTEGER PRIMARY KEY, 
	nombre VARCHAR(60), 
	apellido VARCHAR(60));

CREATE TABLE cliente(
	id INTEGER PRIMARY KEY, 
	cedulaCliente INTEGER, 
	FOREIGN KEY (cedulaCliente) REFERENCES persona(cedula));

CREATE TABLE proveedor(
	cedulaProveedor INTEGER PRIMARY KEY, 
	empresa VARCHAR(60), 
	FOREIGN KEY (cedulaProveedor) REFERENCES persona(cedula));

CREATE TABLE compra(
	id INTEGER PRIMARY KEY, 
	fecha DATE, 
	montoFinal FLOAT, 
	impuesto FLOAT, 
	valorTotal FLOAT, 
	idCliente INTEGER, 
	FOREIGN KEY (idCliente) REFERENCES cliente(id));

CREATE TABLE categoria(
	id INTEGER PRIMARY KEY, 
	nombre varchar(60), 
	descripcion VARCHAR(300));

CREATE TABLE producto(
	id INTEGER PRIMARY KEY, 
	nombre VARCHAR(60), 
	precio FLOAT, stock INTEGER, 
	cedulaProveedor INTEGER, 
	idCategoria INTEGER, 
	FOREIGN KEY (cedulaProveedor) REFERENCES proveedor(cedulaProveedor), 
	FOREIGN KEY (idCategoria) REFERENCES categoria(id));

CREATE TABLE detalle_compra(
	id INTEGER PRIMARY KEY, 
	cantidad int, 
	valorUnitario FLOAT, 
	idCompra INTEGER, 
	idProducto INTEGER, 
	FOREIGN KEY (idCompra) REFERENCES compra(id), 
	FOREIGN KEY (idProducto) REFERENCES producto(id));

CREATE TABLE direccion(
	id INTEGER PRIMARY KEY, 
	ciudad varchar(80), 
	manzana VARCHAR(20), 
	villa VARCHAR(20), 
	ciudadela VARCHAR(30), 
	calle VARCHAR(30), 
	cedula INTEGER, 
	FOREIGN KEY (cedula) REFERENCES persona(cedula));

CREATE TABLE telefono(
	numeroTelefono INTEGER PRIMARY KEY, 
	cedula INTEGER, 
	FOREIGN KEY (cedula) REFERENCES persona(cedula)); 

CREATE TABLE usuario(
	id INTEGER PRIMARY KEY, 
	nombre varchar(10)
	contrasena varchar(12));
