# Insertar valores en persona
INSERT INTO persona(cedula, nombre, apellido) VALUES (0924556390, "Aaron", "Carrasco");
INSERT INTO persona(cedula, nombre, apellido) VALUES (0945781234, "Miguel", "Torres");
INSERT INTO persona(cedula, nombre, apellido) VALUES (0977449536, "Angela", "Carrion");
INSERT INTO persona(cedula, nombre, apellido) VALUES (0924336291, "Christian", "Guanoluisa");
INSERT INTO persona(cedula, nombre, apellido) VALUES (0999334461, "Angel", "Alvarado");
INSERT INTO persona(cedula, nombre, apellido) VALUES (0989831001, "David", "Salazar");
INSERT INTO persona(cedula, nombre, apellido) VALUES (0989831000, "Mara", "Falconi");
INSERT INTO persona(cedula, nombre, apellido) VALUES (0989864552, "Guillermo", "Garcia");
INSERT INTO persona(cedula, nombre, apellido) VALUES (0923748950, "Julio", "Guilindo");
INSERT INTO persona(cedula, nombre, apellido) VALUES (0987845112, "Christina", "Torres");

INSERT INTO persona(cedula) VALUES (0924336292);
INSERT INTO persona(cedula) VALUES (0924336293);
INSERT INTO persona(cedula) VALUES (0924336294);
INSERT INTO persona(cedula) VALUES (0924336295);
INSERT INTO persona(cedula) VALUES (0924336296);
INSERT INTO persona(cedula) VALUES (0924336297);
INSERT INTO persona(cedula) VALUES (0924336298);
INSERT INTO persona(cedula) VALUES (0924336299);
INSERT INTO persona(cedula) VALUES (0924336290);
INSERT INTO persona(cedula) VALUES (0924336291);

# Insertar valores en cliente
INSERT INTO cliente(id, cedulaCliente) VALUES (1, 0924556390);
INSERT INTO cliente(id, cedulaCliente) VALUES (2, 0945781234);
INSERT INTO cliente(id, cedulaCliente) VALUES (3, 0977449536);
INSERT INTO cliente(id, cedulaCliente) VALUES (4, 0924336291);
INSERT INTO cliente(id, cedulaCliente) VALUES (5, 0999334461);
INSERT INTO cliente(id, cedulaCliente) VALUES (6, 0989831001);
INSERT INTO cliente(id, cedulaCliente) VALUES (7, 0989831000);
INSERT INTO cliente(id, cedulaCliente) VALUES (8, 0989864552);
INSERT INTO cliente(id, cedulaCliente) VALUES (9, 0923748950);
INSERT INTO cliente(id, cedulaCliente) VALUES (10, 0987845112);

# Insertar valores en proveedor
INSERT INTO proveedor(cedulaProveedor, empresa) VALUES(0924336290, "Distribuidora PACTE");
INSERT INTO proveedor(cedulaProveedor, empresa) VALUES(0924336291, "Distribuidora MIGUEL SA");
INSERT INTO proveedor(cedulaProveedor, empresa) VALUES(0924336292, "Distribuidora IMRELEVSA");
INSERT INTO proveedor(cedulaProveedor, empresa) VALUES(0924336293, "Distribuidora PICASO");
INSERT INTO proveedor(cedulaProveedor, empresa) VALUES(0924336294, "Distribuidora MIGUELITOS");
INSERT INTO proveedor(cedulaProveedor, empresa) VALUES(0924336295, "Distribuidora CORTADOS");
INSERT INTO proveedor(cedulaProveedor, empresa) VALUES(0924336296, "Distribuidora SISTEMATICS");
INSERT INTO proveedor(cedulaProveedor, empresa) VALUES(0924336297, "Distribuidora VALUS");
INSERT INTO proveedor(cedulaProveedor, empresa) VALUES(0924336298, "Distribuidora THE THINGS");
INSERT INTO proveedor(cedulaProveedor, empresa) VALUES(0924336299, "Distribuidora PITCHERS");

# Insertar valores en telefono
INSERT INTO telefono(numeroTelefono, cedula) VALUES (042427800, 0924556390);
INSERT INTO telefono(numeroTelefono, cedula) VALUES (042427801, 0945781234);
INSERT INTO telefono(numeroTelefono, cedula) VALUES (042427802, 0977449536);
INSERT INTO telefono(numeroTelefono, cedula) VALUES (042427803, 0924336291);
INSERT INTO telefono(numeroTelefono, cedula) VALUES (042427804, 0999334461);
INSERT INTO telefono(numeroTelefono, cedula) VALUES (042427805, 0989831001);
INSERT INTO telefono(numeroTelefono, cedula) VALUES (042427806, 0989831000);
INSERT INTO telefono(numeroTelefono, cedula) VALUES (042427807, 0989864552);
INSERT INTO telefono(numeroTelefono, cedula) VALUES (042427808, 0923748950);
INSERT INTO telefono(numeroTelefono, cedula) VALUES (042427809, 0987845112);

# Insertar valores en direccion
INSERT INTO direccion(id, ciudad, manzana, villa, ciudadela, calle, cedula) 
	VALUES (1, "Guayaquil", "3A", "75B", "Los Esteros", "Cayetano Tarruel", 0924556390);
INSERT INTO direccion(id, ciudad, manzana, villa, ciudadela, calle, cedula) 
	VALUES (2, "Guayaquil", "4A", "115A", "Los Esteros", "Cayetano Tarruel", 0945781234);
INSERT INTO direccion(id, ciudad, manzana, villa, ciudadela, calle, cedula) 
	VALUES (3, "Guayaquil", "2A", "5A", "Floresta", "Miguel Antonio", 0977449536);
INSERT INTO direccion(id, ciudad, manzana, villa, ciudadela, calle, cedula) 
	VALUES (4, "Guayaquil", "B", "75B", "Lirios", "Angela De Las Mercedes", 0924336291);
INSERT INTO direccion(id, ciudad, manzana, villa, ciudadela, calle, cedula) 
	VALUES (5, "Guayaquil", "B", "77B", "Lirios", "Angela De Las Mercedes", 0999334461);
INSERT INTO direccion(id, ciudad, manzana, villa, ciudadela, calle, cedula) 
	VALUES (6, "Guayaquil", "C", "2C", "Lirios", "Angela De Las Mercedes", 0989831001);
INSERT INTO direccion(id, ciudad, manzana, villa, ciudadela, calle, cedula) 
	VALUES (7, "Guayaquil", "C", "9C", "Lirios", "Angela De Las Mercedes", 0989831000);
INSERT INTO direccion(id, ciudad, manzana, villa, ciudadela, calle, cedula) 
	VALUES (8, "Guayaquil", "D", "20D", "Lirios", "Angela De Las Mercedes", 0989864552);
INSERT INTO direccion(id, ciudad, manzana, villa, ciudadela, calle, cedula) 
	VALUES (9, "Guayaquil", "32A", "B15", "Santa Monica", "Luis Pabellon", 0923748950);
INSERT INTO direccion(id, ciudad, manzana, villa, ciudadela, calle, cedula) 
	VALUES (10, "Guayaquil", "32A", "B18", "Santa Monica", "Luis Pabellon", 0987845112);

# Insertar en categoria
INSERT INTO categoria(id, nombre, descripcion) VALUES (1, "Oficina", "Articulos para personal de oficina");
INSERT INTO categoria(id, nombre, descripcion) VALUES (2, "Escolar", "Articulos varios para la vida estudiantil");
INSERT INTO categoria(id, nombre, descripcion) VALUES (3, "Escolar Libros", "Material didactico para estudiantes");
INSERT INTO categoria(id, nombre, descripcion) VALUES (4, "Recepcion", "Materiales para gente de recepcion (como campanas)");
INSERT INTO categoria(id, nombre, descripcion) VALUES (5, "Jefes", "Articulos personalizados para jefes de oficinas");
INSERT INTO categoria(id, nombre, descripcion) VALUES (6, "Papeleria Varia", "Articulos de papeleria varios");
INSERT INTO categoria(id, nombre, descripcion) VALUES (7, "Pinturas", "Pinturas de varios colores");
INSERT INTO categoria(id, nombre, descripcion) VALUES (8, "Retratos", "Portaretratos y materiales para realizar uno");
INSERT INTO categoria(id, nombre, descripcion) VALUES (9, "Maderas", "Madera fina para realizar trabajos escolares");
INSERT INTO categoria(id, nombre, descripcion) VALUES (10, "Hojas", "Hojas de todo tipo para realizacion de trabajos");

# Insertar en producto
INSERT INTO producto(id, nombre, precio, stock, cedulaProveedor, idCategoria) 
	VALUES (1, "Grapadora", 6.50, 5, 0924336290, 1);
INSERT INTO producto(id, nombre, precio, stock, cedulaProveedor, idCategoria) 
	VALUES (2, "Pluma Negra", 0.50, 15, 0924336291, 2);
INSERT INTO producto(id, nombre, precio, stock, cedulaProveedor, idCategoria)
	VALUES (3, "Matematicas 5to", 25.79, 3, 0924336292, 3);
INSERT INTO producto(id, nombre, precio, stock, cedulaProveedor, idCategoria)
	VALUES (4, "Campana Recepcion", 20.00, 1, 0924336293, 4);
INSERT INTO producto(id, nombre, precio, stock, cedulaProveedor, idCategoria)
	VALUES (5, "Vasos Personalizados", 9.49, 30, 0924336294, 5);
INSERT INTO producto(id, nombre, precio, stock, cedulaProveedor, idCategoria)
	VALUES (6, "Sacadora de Grapas", 1.25, 6, 0924336295, 6);
INSERT INTO producto(id, nombre, precio, stock, cedulaProveedor, idCategoria)
	VALUES (7, "Pintura Oleo Blanca", 13.50, 2, 0924336296, 7);
INSERT INTO producto(id, nombre, precio, stock, cedulaProveedor, idCategoria)
	VALUES (8, "Retrato de Pino Fino", 35.10, 1, 0924336297, 8);
INSERT INTO producto(id, nombre, precio, stock, cedulaProveedor, idCategoria)
	VALUES (9, "Madera fina para maquetas", 3.50, 10, 0924336298, 9);
INSERT INTO producto(id, nombre, precio, stock, cedulaProveedor, idCategoria)
	VALUES (10, "Hojas A3", 0.95, 300, 0924336299, 10);

# Insertar en compra
INSERT INTO compra(id, fecha, montoFinal, impuesto, valorTotal, idCliente)
	VALUES(1, now(), 6.50, 12.0, 7.28, 1);
INSERT INTO compra(id, fecha, montoFinal, impuesto, valorTotal, idCliente)
	VALUES(2, now(), 0.50, 12.0, 0.56, 2);
INSERT INTO compra(id, fecha, montoFinal, impuesto, valorTotal, idCliente)
	VALUES(3, now(), 25.79, 12.0, 28.88, 3);
INSERT INTO compra(id, fecha, montoFinal, impuesto, valorTotal, idCliente)
	VALUES(4, now(), 20.00, 12.0, 22.4, 4);
INSERT INTO compra(id, fecha, montoFinal, impuesto, valorTotal, idCliente)
	VALUES(5, now(), 9.49, 12.0, 10.63, 5);
INSERT INTO compra(id, fecha, montoFinal, impuesto, valorTotal, idCliente)
	VALUES(6, now(), 1.25, 12.0, 1.40, 6);
INSERT INTO compra(id, fecha, montoFinal, impuesto, valorTotal, idCliente)
	VALUES(7, now(), 13.50, 12.0, 15.12, 7);
INSERT INTO compra(id, fecha, montoFinal, impuesto, valorTotal, idCliente)
	VALUES(8, now(), 35.10, 12.0, 39.31, 8);
INSERT INTO compra(id, fecha, montoFinal, impuesto, valorTotal, idCliente)
	VALUES(9, now(), 3.50, 12.0, 3.92, 9);
INSERT INTO compra(id, fecha, montoFinal, impuesto, valorTotal, idCliente)
	VALUES(10, now(), 0.95, 0.0, 0.95, 10);
INSERT INTO compra(id, fecha, montoFinal, impuesto, valorTotal, idCliente)
	VALUES(11, now(), 0.95, 0.0, 0.95, 1);

# Insertar en detalle de compra
INSERT INTO detalle_compra(id, cantidad, valorUnitario, idCompra, idProducto) VALUES(1, 1, 6.50, 1, 1);
INSERT INTO detalle_compra(id, cantidad, valorUnitario, idCompra, idProducto) VALUES(2, 1, 0.50, 2, 2);
INSERT INTO detalle_compra(id, cantidad, valorUnitario, idCompra, idProducto) VALUES(3, 1, 25.79, 3, 3);
INSERT INTO detalle_compra(id, cantidad, valorUnitario, idCompra, idProducto) VALUES(4, 1, 20.00, 4, 4);
INSERT INTO detalle_compra(id, cantidad, valorUnitario, idCompra, idProducto) VALUES(5, 1, 9.49, 5, 5);
INSERT INTO detalle_compra(id, cantidad, valorUnitario, idCompra, idProducto) VALUES(6, 1, 1.25, 6, 6);
INSERT INTO detalle_compra(id, cantidad, valorUnitario, idCompra, idProducto) VALUES(7, 1, 13.50, 7, 7);
INSERT INTO detalle_compra(id, cantidad, valorUnitario, idCompra, idProducto) VALUES(8, 1, 35.10, 8, 8);
INSERT INTO detalle_compra(id, cantidad, valorUnitario, idCompra, idProducto) VALUES(9, 1, 3.50, 9, 9);
INSERT INTO detalle_compra(id, cantidad, valorUnitario, idCompra, idProducto) VALUES(10, 1, 0.95, 10, 10);
