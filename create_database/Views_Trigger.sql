# Muestra el cliente mas frecuente
DROP VIEW IF EXISTS cliente_frecuente;
CREATE VIEW cliente_frecuente AS SELECT idCliente, COUNT(idCliente) AS total FROM compra GROUP BY idCliente ORDER BY total DESC LIMIT 1;

# Muestra los productos y cantidad de clientes que compraron el producto
DROP VIEW IF EXISTS producto_cantidadcliente;
CREATE VIEW producto_cantidadcliente AS SELECT p.nombre, COUNT(idCliente) AS total FROM compra INNER JOIN detalle_compra dt ON compra.id = dt.idCompra INNER JOIN producto p ON dt.idProducto = p.id GROUP BY p.nombre ORDER BY total DESC;

# Producto menos vendido 
DROP VIEW IF EXISTS producto_menosven;
CREATE VIEW producto_menosven AS SELECT p.nombre, COUNT(idCliente) AS total FROM compra INNER JOIN detalle_compra dt ON compra.id = dt.idCompra INNER JOIN producto p ON dt.idProducto = p.id GROUP BY p.nombre ORDER BY total ASC LIMIT 1;

# Categoria con mas productos vendidos
DROP VIEW IF EXISTS categoria_productoven;
CREATE VIEW categoria_productoven AS SELECT c.nombre , COUNT(idCliente) AS total FROM compra INNER JOIN detalle_compra dt ON compra.id = dt.idCompra INNER JOIN producto p ON dt.idProducto = p.id INNER JOIN categoria c ON p.idCategoria = c.id GROUP BY c.nombre ORDER BY total DESC;

# Cantidad de productos ordenados por proveedor
DROP VIEW IF EXISTS productos_proveedor;
CREATE VIEW productos_proveedor AS SELECT p.nombre, p.stock, pro.empresa FROM producto p INNER JOIN proveedor pro ON p.cedulaProveedor = pro.cedulaProveedor;

# Productos mas vendidos (5)
DROP VIEW IF EXISTS producto_masven;
CREATE VIEW producto_masven AS SELECT p.nombre, COUNT(idCliente) AS total FROM compra INNER JOIN detalle_compra dt ON compra.id = dt.idCompra INNER JOIN producto p ON dt.idProducto = p.id GROUP BY p.nombre ORDER BY total DESC LIMIT 5;

# Indice
CREATE INDEX idx_productos ON producto(nombre, stock, cedulaProveedor);

# Trigger
DROP TRIGGER IF EXISTS fix_date;
DELIMITER **
CREATE TRIGGER fix_date BEFORE INSERT ON compra FOR EACH ROW 
BEGIN
SET NEW.fecha = now();
END
** DELIMITER ;

# Procedimiento
DROP PROCEDURE IF EXISTS proc_calculariva;
DELIMITER ** 
CREATE PROCEDURE proc_calculariva(IN id_in INT, OUT resultado DECIMAL)
BEGIN
 
SELECT montoFinal * 1.12 FROM compra c WHERE c.id = id_in INTO resultado;
 
END
** DELIMITER ;

